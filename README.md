![oee.ai Banner](https://gitlab.com/ifp-software/node-red-contrib-oee-ai-connector/-/raw/main/images/banner.jpg)

# oee.ai Node-RED Integration

oee.ai is a real-time, plug-and-play, minimally-invasive Software-as-a-Service manufacturing integration platform to visualize and improve the Overall Equipment Effectiveness (OEE) of industrial production machines.

The foundation of this analysis is based on the data `produced units over time`, i.e. how much output was created during a defined period of time (typically every 30 seconds). This data can be aquired in a variety of ways and depends on the type and capabilities of the production machine to be connected.

For machines that do not provide any network connectivity, oee.ai offers minimally-invasive hardware sensors in combination with SICK TDC gateways running a Node-RED instance. Newer or retrofitted equipment on the other hand oftentimes provides some sort of connectivity itself (e.g. via OPC-UA, Modbus, PLC, or similar) that can be accessed via Node-RED. Both possible data flows are supported by this package through custom nodes.

# Prerequisites

1. A source of data which can be either:
   1. A set of hardware sensors connected to a SICK TDC gateway running Node-RED (provided by oee.ai) *or*
   2. An industrial PC (IPC) or other on-premise device that can access the machine’s connection interface, has internet access to the oee.ai MQTT broker, and can run Node-RED. Please consult that device’s documentation on how to read the number of produced units from the production machine via the available connection.

2. An account for oee.ai. Please contact us at info@oee.ai to get in touch with us.
3. The sensor ID and token for the machine to be connected. These can be found in the oee.ai web application in the menu `Machine -> Sensors`.

# Installation

To install this package, navigate to the top right menu in Node-RED, click `Manage Palette` and select the `Install` tab. Search for `@ifp-software/node-red-contrib-oee-ai-connector` and click the `Install` button.

# Nodes

![Nodes](https://gitlab.com/ifp-software/node-red-contrib-oee-ai-connector/-/raw/main/images/nodes.png)

This package currently includes two nodes – `oee.ai Connector` and `oee.ai SICK TDC Connector`. Both can be found in the `network` section of the list of available nodes on the left in Node-RED.

## oee.ai SICK TDC Connector

> This node is only required if a SICK TDC gateway running Node-RED is used to gather data from connected hardware sensors. If this is not the case for the machine to be connected, you can safely skip this section.

The purpose of this node is to aggregate data sent by the SICK TDC gateway among 30- or 60-second intervals before it is sent to oee.ai using the [oee.ai connector node](#oeeai-connector). Either output of this node can simply be connected to an oee.ai connector node without having to worry about the message properties it expects (see the provided [examples](#examples)).

The aggregation interval (30 or 60 seconds) can be configured by editing the node.

### Inputs

The only message property expected by this node is `msg.payload`. It is supposed to contain a string representing a JSON object holding the properties `Value` and `DioName`. As this is the format a SICK TDC gateway provides its gathered data in for digital inputs and outputs, this requirement is easily satisfied by connecting the output of a WebSocket node (available on Node-RED by default) polling data from the gateway to the input of a SICK TDC connector node. This WebSocket node has to be configured with the WebSocket URL exposed by the SICK TDC gateway and can look like this:

```
ws://192.168.0.100:32869/ws/tdce/dio/states
```

The correct IP address and port for the WebSocket connection can be obtained from the configuration of the SICK TDC device. Note that you have to be connected to the device’s network for a WebSocket connection to be established.

See the provided [examples](#examples) and the documentation of the node in Node-RED for more information on how to set up such a flow.

### Outputs

The node provides six outputs corresponding to the six digital inputs of a SICK TDC gateway. Each of these outputs can be directly connected to an [oee.ai connector node](#oeeai-connector). For example, if the first sensor connected to the gateway measures the amount of units produced by the machine and the second sensor connected to the gateway measures the amount of rejected units, it makes sense to connect the first two outputs of this node to distinct oee.ai connector nodes. These connector nodes can then be configured to send data as different logical sensors defined for the machine in the oee.ai web application, one for measuring production output and one for measuring rejects.

## oee.ai Connector

The purpose of this node is to send production data to oee.ai.

The node can be configured with a sensor ID and sensor token as well as the oee.ai instance to connect to (`preview` for development and testing, or `production` for live machine data).

The message properties expected by this node are:

| Property    | Type      | Description                                        | Default               |
| :---------- | --------- | -------------------------------------------------- | --------------------- |
| `msg.count` | number    | The number of produced units during the timeframe. | `0`                   |
| `msg.to`    | timestamp | The end of the timeframe.                          | `now`                 |
| `msg.from`  | timestamp | The beginning of the timeframe.                    | `msg.to - 30 seconds` |

All properties are optional, but at least `msg.count` is required for meaningful use. Also, the messages must be sent in chronological order, meaning if newer data exists in the oee.ai time series database, incoming older measurements will be discarded.

The node sits at the end of a flow and takes care of formatting the payload into the required JSON document, creating and maintaining the connection to the oee.ai MQTT broker, and sending the formatted payload to the sensor’s MQTT topic every time it is triggered.

### Connection details

The connector node can send data to the oee.ai preview system (used for development and testing) or to the production system (used for live machine data). The device running Node-RED (e.g. IPC or SICK TDC gateway) must be able to connect to the oee.ai MQTT broker using the connection settings below.

| Server            | URL                   | IP                |
| :---------------- | :-------------------- | ----------------- |
| Preview (Testing) | `mqtt.preview.oee.ai` | `46.101.142.152`  |
| Production        | `mqtt.oee.ai`         | `174.138.102.247` |

The connection can be established via TCP or WebSocket and on different ports. Encrypted communication via WebSocket and Port 443 is recommended, as it usually does not require any special configuration of firewalls and proxies. Unencrypted connections are also available as a fallback.

| Protocol         | Port        |  Encryption  |
| :--------------- | :---------- | :----------: |
| TCP              | 1883        | 🚫           |
| TCP + SSL        | 8883        | 🔒           |
| WebSocket        | 80 or 8083  | 🚫           |
| WebSocket Secure | 443 or 8084 | 🔒           |

Accounts for the web application are the same for preview and production, but have to be enabled for each system separately. Please contact us at info@oee.ai to get access.

Sensors (with their ID and token) are not interchangeable between the two instances; sensors configured on the preview system cannot send data to the production system, and vice versa.

# Examples

This package contains multiple example flows that demonstrate typical use cases for the different nodes provided.

Every example can be imported into an existing or a new flow by opening the Node-RED menu in the top right corner and clicking on `Import`. In the `Import nodes` dialog, open the `Examples` tab on the left and navigate to `Examples -> flows -> @ifp-software/node-red-contrib-oee-ai-connector`. Then select one of the available examples to import.

## Sending a random number

![Example flow "Sending a random number"](https://gitlab.com/ifp-software/node-red-contrib-oee-ai-connector/-/raw/main/images/example-random-number.png)

This example shows how the number of produced units during a given timeframe (typically every 30 seconds) can be sent to oee.ai if it can be read from the production machine directly (i.e. not as a running total). The actual implementation of getting this data from the machine depends on the equipment and is left to the knowledgeable user. In this example it is represented by a `function` node generating a random number between 75 and 100 every time it is triggered.

## Sending the difference of a running total

![Example flow "Sending the difference of a running total"](https://gitlab.com/ifp-software/node-red-contrib-oee-ai-connector/-/raw/main/images/example-running-total.png)

This example shows how the number of produced units during a given timeframe (typically every 30 seconds) can be sent to oee.ai if the data read from the machine is a running total. The actual implementation of getting this data from the machine depends on the equipment and is left to the knowledgeable user. In this example it is represented by a `function` node that adds a random number between 75 and 100 to a running total stored in its own context and sends this ever increasing number as `msg.count`.

The second `function` node stores the previously received `msg.count` in its context and sends the difference to the currently received `msg.count` to the connector node. In case the currently received `msg.count` is less than the previously remembered number it sends the unaltered count to the next node.

## Sending data using a SICK TDC gateway

![Example flow "Sending data using a SICK TDC gateway"](https://gitlab.com/ifp-software/node-red-contrib-oee-ai-connector/-/raw/main/images/example-sick-tdc.png)

This example flow shows how to gather data from a SICK TDC gateway and send it to oee.ai. The flow consists of three nodes, the first of which being a WebSocket node polling data from a SICK TDC gateway. This node has to be configured with the WebSocket URL exposed by the gateway as explained in the [inputs section](#inputs) of the SICK TDC connector node.

The second node is an [oee.ai SICK TDC connector](#oeeai-sick-tdc-connector) aggregating the received data among 30- or 60-second intervals. This interval can be configured by editing the node. The third node sends the data gathered for the first digital input of the TDC device to oee.ai.

## Sending data using a SICK TDC gateway with rate limiting

![Example flow "Sending data using a SICK TDC gateway with rate limiting"](https://gitlab.com/ifp-software/node-red-contrib-oee-ai-connector/-/raw/main/images/example-sick-tdc-rate-limiting.png)

This example flow presents the same process as the previous example but additionally employs rate limiting after receiving data from the SICK TDC gateway via WebSocket. This can be useful in scenarios where the sensors connected to the gateway frequently record unwanted short-lived signals caused by environmental circumstances and not by actual production, thus distorting the total amount of produced units.

The first added node is a change node setting the allowed rate *t* in milliseconds (meaning that the rate of messages is limited to one per *t* milliseconds). The second added node takes care of the actual rate limiting by discarding every message received less than *t* milliseconds after the message before was received.

## Next steps

To complete an example, enter a sensor’s ID and token found in the oee.ai web application into an oee.ai connector node’s configuration and deploy the flow. If the connector node then shows the “connected” status, measurements will appear shortly after on the oee.ai web application, specifically under `Machine -> Measurements`.

## Advanced integration with oee.ai

This package also contains example flows which work independently from the provided connector nodes and showcase other common examples of one-way synchronization from an arbitrary system to oee.ai, such as:

- Changing products
- Responding to loss-alerts
- Sending measurements
- Synchronizing products
- Synchronizing loss reasons
- Synchronizing production orders
- Synchronizing shifts
- Integrating the MPDV Manufacturing Integration Platform

Keep in mind that these more advanced use cases utilize our REST API and require authentication using an API token with **read and write access**. API tokens can be created within our web application under `Location -> API tokens`.

Of course, our REST API can be used to realize many more integration scenarios in Node-RED. Feel free to consult our [API documentation](https://docs.oee.ai) for that purpose. Contact us at info@oee.ai to request access for your oee.ai account.

# Troubleshooting / FAQ

## Why doesn’t the connector node connect to oee.ai?

If an oee.ai connector node does not switch to the “connected” status after deploying the flow, please check that the sensor ID and token are entered correctly and the selected server matches that of your oee.ai account.

Remaining connection problems can almost always be traced back to some network firewall blocking access to the MQTT broker. Please ensure that the device running Node-RED can connect to the oee.ai MQTT broker using the the IP address and port [listed above](#connection-details).

## Why are all measurements sent to oee.ai zero?

If measurements are successfully sent to oee.ai, but do not show the produced output, most likely the oee.ai connector node used receives messages without a `count` attribute set. Please ensure that after reading the production output of your equipment this number is written to `msg.count`.

If you are using a SICK TDC connector node, please check that the WebSocket node used to connect to the SICK TDC gateway is correctly configured and shows the “connected” status. Note that you have to be connected to the gateway’s network for a WebSocket connection to be established.

## Is it possible to send data more frequently?

There is no benefit in sending data more frequently than every 30 seconds. Analysis and reporting in oee.ai has a resolution of one minute, so all incoming data is assigned proportionally to full minutes on the wall clock. Additionally, every message sent to the oee.ai MQTT broker starts a variety of background tasks to analyze and pre-calculate KPIs, so limiting the incoming message frequency reduces the load on the whole system.

## Contact information

If you have any questions regarding the connectivity of your equipment to oee.ai, please get in touch with us:

```
ifp Software GmbH
Eupener Straße 22
52066 Aachen
Germany

info@oee.ai
```
