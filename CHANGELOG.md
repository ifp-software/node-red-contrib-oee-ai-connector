# oee.ai Node-RED connector changelog

## 2.13.0

* Implements new licensing over unix socket
* Implements 4h Engineering License for Bosch Rexroth CtrlX Virtual Core
* All files formatted with prettier

## 2.12.0

* Add example flow for connecting SQL databases

## 2.11.0

* Update MIP flows to MIP 2.0

## 2.10.0

* Add support for all ports on both preview and production MQTT brokers
  * Port 80 (WebSocket)
  * Port 443 (WebSocket Secure)
  * Port 1883 (TCP)
  * Port 8083 (WebSocket)
  * Port 8084 (WebSocket Secure)
  * Port 8883 (TCP + SSL)

## 2.9.0

* Extend SICK TDC connector node to support all six digital inputs of a SICK TDC gateway

## 2.8.0

* Add proxy username and password fields

## 2.7.1

* Update loss alert MQTT notification topic in example flows
* Fix "incomplete credentials" status propagation to Node-RED status nodes

## 2.7.0

* Don't try to conncet to MQTT broker when no credentials are provided
* Try reconnecting after 60 seconds after fatal broker errors occured
* Better logging of MQTT events
* Update example flow for synchronizing products, loss reasons, and shifts
* Update example flow for sending a running total difference

## 2.6.7

* Add synchronize shifts flow for MIP

## 2.6.6

* Add proper migration logic for the protocol and proxy fields

## 2.6.5

* Add JSESSION cookie to MIP flows

## 2.6.4

* Added MIP flow for products

## 2.6.3

* Added MIP flow for loss reasons

## 2.6.2

* Rename MIP example directory

## 2.6.1

* Updates and fix for README.md

## 2.6.0

* Add example flow for 'changing product or line items'
* Add example flow for 'responding to loss alerts'

## 2.5.0

* Add example flows for MIP integration

## 2.4.0

* Connect using TCP or WSS to MQTT broker
* Add (optional) proxy settings

## 2.3.0

* Add synchronize loss reasons example flow
* Add synchronize production orders example flow
* Add synchronize products example flow
* Add synchronize shifts example flow

## 2.2.1

* Minor fixes to documentation and screenshots

## 2.2.0

* Update MQTT dependency
* Disable ping rescheduling for MQTT client
* Add SICK TDC connector node and documentation

## 2.1.1

* Fix creation of Date object to when msg.to is undefined

## 2.1.0

* Add Node-RED and Node.js engine versions to package.json
* Update MQTT dependency

## 2.0.1

* Improve detection of Bosch ctrlX CORE control systems

## 2.0.0

* Check for license when running on a Bosch ctrX CORE control system

## 1.0.3

* End MQTT client connection on authentication errors

## 1.0.2

* End MQTT client connection on error

## 1.0.1

* Static links to Gitlab hosted images in `README.md`
