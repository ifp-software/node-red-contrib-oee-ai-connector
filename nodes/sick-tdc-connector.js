module.exports = function (RED) {
  "use strict";

  /**
   * SICK TDC gateway aggregator node for {@link https://oee.ai|oee.ai}.
   * @param config User configuration of the node.
   * @constructor
   */
  function OeeAiSickTdcConnectorNode(config) {
    RED.nodes.createNode(this, config);

    const interval = parseInt(config.interval);
    let counts = { DIO_A: 0, DIO_B: 0, DIO_C: 0, DIO_D: 0, DIO_E: 0, DIO_F: 0};

    // Whenever a message reaches the node
    this.on('input', (msg, send, done) => {
      // Extract value and DIO name
      let value, input;
      const json = JSON.parse(msg.payload);
      value = json["Value"];
      input = json["DioName"];

      // Increment global count if signal was detected and input is valid
      if (value === 1) {
        if (!Object.keys(counts).includes(input)) {
          done(RangeError(`Received signal for invalid input '${input}'`));
          return;
        }
        counts[input]++;
      }
      done();
    });

    // Every `interval` seconds
    const loop = setInterval(() => {
      const to = new Date();  // now
      const from = new Date(to.valueOf() - interval * 1000);

      this.send(Object.keys(counts).map(input => {
        const count = counts[input];
        counts[input] = 0;  // reset count to zero
        return { count, from, to };
      }));
    }, interval * 1000);

    // When the node is shut down or replaced
    this.on("close", () => {
      clearInterval(loop);
    });
  }

  RED.nodes.registerType("oee-ai-sick-tdc-connector", OeeAiSickTdcConnectorNode);
};
